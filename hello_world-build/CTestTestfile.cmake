# CMake generated Testfile for 
# Source directory: H:/subjects/game_new/assignment_2/hello_world
# Build directory: H:/subjects/game_new/assignment_2/hello_world-build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(mylibrary)
subdirs(benchmarks)
subdirs(report_selection_sort)
subdirs(report_introsort)
