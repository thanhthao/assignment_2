# CMake generated Testfile for 
# Source directory: H:/subjects/game_new/assignment_2/hello_world/mylibrary
# Build directory: H:/subjects/game_new/assignment_2/hello_world-build/mylibrary
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Algorithm_ShellSort.SortTest_01 "test_shellsort_tests" "--gtest_filter=Algorithm_ShellSort.SortTest_01")
add_test(Algorithm_SelectionSort.BasicSort "test_introsort_tests" "--gtest_filter=Algorithm_SelectionSort.BasicSort")
add_test(Algorithm_SelectionSort.BasicSortNegativeValues "test_introsort_tests" "--gtest_filter=Algorithm_SelectionSort.BasicSortNegativeValues")
add_test(Container_Vector.Initialization "test_vector_tests" "--gtest_filter=Container_Vector.Initialization")
add_test(Container_Vector.Move "test_vector_tests" "--gtest_filter=Container_Vector.Move")
add_test(Container_Vector.Iterator "test_vector_tests" "--gtest_filter=Container_Vector.Iterator")
add_test(Container_Vector.push_back "test_vector_tests" "--gtest_filter=Container_Vector.push_back")
add_test(Container_Vector.Range "test_vector_tests" "--gtest_filter=Container_Vector.Range")
add_test(Container_List.Size_functionality "test_list_tests" "--gtest_filter=Container_List.Size_functionality")
